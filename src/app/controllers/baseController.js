/*
|| What should I Eat Tonight?
||---------------------------
|| Author: Dan Dvoracek
|| Description: Makes the recipes available to its scope 
*/

(function() {

	var BaseController = function($scope, imagesFactory) {

		$scope.getImage = imagesFactory.getImages();

	};

	BaseController.$inject = ['$scope', 'imagesFactory'];

	angular.module('someApp').controller('BaseController', BaseController);

}());



