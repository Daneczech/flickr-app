/*
|| What should I Eat Tonight?
||---------------------------
|| Author: Dan Dvoracek
|| Description: Makes the recipes available to its scope 
*/

(function() {

	var SingleController = function($scope, $routeParams, imagesFactory) {

		// the customer id comes from the routing file
        var pictureId = $routeParams.picId;

        // console.log(parseInt(pictureId));

		$scope.getImage = imagesFactory.getSingleImage(pictureId);

	};

	SingleController.$inject = ['$scope', '$routeParams', 'imagesFactory'];

	angular.module('someApp').controller('SingleController', SingleController);

}());



