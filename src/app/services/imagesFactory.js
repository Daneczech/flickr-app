/*
|| Potato Task
||---------------------------
|| Author: Dan Dvoracek
|| Description: Factory containing methods to query flickr api
*/

(function() {

	var imagesFactory = function($q) {

		var factory = {},
			imgLinks = {};
		
		// Prepare data
		function queryAPI(){
			// Querystring
			var query = "http://api.flickr.com/services/feeds/photos_public.gne?tags=potato,food&tagmode=all&content_type=1&sort=relevance&format=json";

			// Ajax call to retrieve data
			$.ajax({
			   data:{format: "json"},
			   dataType: "jsonp",
			   url: query               
			});
		}

		// Get a list of images and output them all as a list
		factory.getImages = function(){
			// get data...
			queryAPI();
			
			// This function is called once the call is satisfied
			// http://stackoverflow.com/questions/13854250/understanding-cross-domain-xhr-and-xml-data
			jsonFlickrFeed = function (data) {

			    var html = '';
			    
			    // Start cycling through the array of Flickr photo details
			    $.each(data.items, function(i,item){

			    	// Truncate title if this one is too long...
			    	var titleString = item.title;
					if(titleString.length > 35) titleString = titleString.substring(0,35)+"...";

					// Format the author's name
					var author = item.author;
					author = author.replace(')', '').substring(author.indexOf("(") + 1);

			    	// Format the date in a cleaner way
			    	var formattedDate = item.published.slice(0,-4).replace(/-/g, '.').replace('T', ' at ');

			    	// Prepare the shared Url for the google+1 button
			    	var sharedUrl = 'http://www.dandvoracek.com/web/potato/#/picture/'+i;


			    	// Prepare the html for each image
					html += "<div class='item-row'>";
					html += "<div class='fk-images'><img src='"+item.media.m+"' alt='"+item.title+"' /></div>";
					html += "<a href='#/picture/"+i+"' class='title-link'>"+titleString+"</a><div id='g-wrap'><div class='g-plusone' data-size='medium' data-href='"+sharedUrl+"'></div></div>";
					html += "<div class='flex-wrap'>";
					html += "<p class='date'>Published: "+formattedDate+"</p>";
					html += "<a class='author-link' href='http://www.flickr.com/photos/"+item.author_id+"' target='_blank'>"+author+"</a>";
					html += "<a class='flickr-link' href='"+item.link+"' target='_blank'>View on Flickr</a>";
					html += "</div>";
					html += "</div>";

			    });

			    // Output the html
			    $('#listImages').prepend(html);

			};

		}

		// Get a list of images and output the right one
		factory.getSingleImage = function(pictureId){
			// get data...
			queryAPI();
			
			// This function is called once the call is satisfied
			// http://stackoverflow.com/questions/13854250/understanding-cross-domain-xhr-and-xml-data
			jsonFlickrFeed = function (data) {

				// Declare ooutput variable and which object to look for/in
			    var single = '',
			    	thisImage = data.items[pictureId];

		    	// Truncate title if this one is too long...
		    	var title = thisImage.title;

		    	// Format the author's name
				var author = thisImage.author;
				author = author.replace(')', '').substring(author.indexOf("(") + 1);

		    	// Format the date in a cleaner way
		    	var formattedDate = thisImage.published.slice(0,-4).replace(/-/g, '.').replace('T', ' at ');

		    	// Format the description in a cleaner way
				var elem = $("<div>" + thisImage.description + "</div>"),
					p = elem.last("p"),
					preFormattedDescr = p[0].innerText,
					formattedDescr = preFormattedDescr.substring(preFormattedDescr.indexOf(':') + 2); // +2 because we want to get rid of the semi-colon AND the space right after it.

				// console.log("This is a formatted description: "+formattedDescr);
				if(formattedDescr === ' '){
					formattedDescr = 'There is no description available for this photo.';
				} else {
					formattedDescr = formattedDescr;
				}

				//get the url to share
				var url = window.location.href;

				// Prepare the html for the page
				single += "<a class='flickr-link' href='"+thisImage.link+"' target='_blank'>"+title+"</a><div id='g-wrap'><div class='g-plusone' data-size='medium' data-href='"+url+"'></div></div>";
				single += "<a class='author-link' href='http://www.flickr.com/photos/"+thisImage.author_id+"' target='_blank'>"+author+"</a>";
				single += "<p class='date'>Published: "+formattedDate+"</p>";
				single += "<div class='fk-images'><img src='"+thisImage.media.m+"' alt='"+thisImage.title+"' /></div>";
				single += "<p class='description'>"+formattedDescr+"</p>";
				single += "<div class='tags'><span>Tags:</span> "+thisImage.tags+"</div>";
			    
			    $('#singleImage').html(single);

			};

		}

		return factory;

	};

	angular.module('someApp').factory('imagesFactory', imagesFactory);

}());